//
//  HistoryTableViewController.h
//  ShoppingList_0.1
//
//  Created by kravinov on 12/24/16.
//  Copyright © 2016 kravinov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>


@interface HistoryTableViewController : UITableViewController <UISearchBarDelegate>

@property (strong, nonatomic) NSMutableArray *items;
@property BOOL ll90IsPressed;
@property BOOL dollarIsPressed;
@property BOOL abcIsPressed;
@property BOOL cgtIsPressed;
- (IBAction)abcFilterButton:(id)sender;
- (IBAction)categoryFilterButton:(id)sender;

- (IBAction)ll90FilterButton:(id)sender;
- (IBAction)dollarFilterButton:(id)sender;

@end
