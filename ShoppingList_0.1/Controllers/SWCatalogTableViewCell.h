//
//  SWCatalogTableViewCell.h
//  ShoppingList_0.1
//
//  Created by kravinov on 12/23/16.
//  Copyright © 2016 kravinov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWTableViewCell.h"

@interface SWCatalogTableViewCell : SWTableViewCell
@property (weak, nonatomic) IBOutlet UILabel *titelLable;
@property (weak, nonatomic) IBOutlet UILabel *detailLable;

@end
