//
//  CatalogTableViewController.m
//  ShoppingList_0.1
//
//  Created by kravinov on 12/17/16.
//  Copyright © 2016 kravinov. All rights reserved.
//

#import "CatalogTableViewController.h"

@interface CatalogTableViewController ()

@end

@implementation CatalogTableViewController

#pragma mark - View Controller Lifecycle Methods

- (void)viewDidLoad {
    [super viewDidLoad];
    self.abcIsPressed = YES;
    self.cgtIsPressed = NO;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    NSManagedObjectContext *managedObjectContext = [self managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"Item"];
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"itemName" ascending:YES];
    fetchRequest.sortDescriptors = @[sortDescriptor];
    self.items = [[managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
    [self.tableView reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.items.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];

    NSManagedObject *item = [self.items objectAtIndex:indexPath.row];
    
    cell.textLabel.text = [item valueForKey:@"itemName"];
    cell.detailTextLabel.text = [item valueForKey:@"itemCategory"];
    
    if ([[item valueForKey:@"itemCategory"] isEqualToString:@"Выпечка"]) {
        cell.imageView.image = [UIImage imageNamed:@"bake32.png"];
        cell.backgroundColor = [UIColor colorWithRed:0.82 green:0.41 blue:0.12 alpha:0.25];

    } else if ([[item valueForKey:@"itemCategory"] isEqualToString:@"Морепродукты"]) {
        cell.imageView.image = [UIImage imageNamed:@"seafood32.png"];
        cell.backgroundColor = [UIColor colorWithRed:0.30 green:0.35 blue:0.40 alpha:0.25];

    } else if ([[item valueForKey:@"itemCategory"] isEqualToString:@"Фрукты"]) {
        cell.imageView.image = [UIImage imageNamed:@"fruits32.png"];
        cell.backgroundColor = [UIColor colorWithRed:0.89 green:0.15 blue:0.21 alpha:0.25];

    } else if ([[item valueForKey:@"itemCategory"] isEqualToString:@"Напитки"]) {
        cell.imageView.image = [UIImage imageNamed:@"beverages32.png"];
        cell.backgroundColor = [UIColor colorWithRed:0.18 green:0.55 blue:0.34 alpha:0.25];

    } else if ([[item valueForKey:@"itemCategory"] isEqualToString:@"Мясо"]) {
        cell.imageView.image = [UIImage imageNamed:@"meat32.png"];
        cell.backgroundColor = [UIColor colorWithRed:0.65 green:0.15 blue:0.04 alpha:0.25];

    } else if ([[item valueForKey:@"itemCategory"] isEqualToString:@"Овощи"]) {
        cell.imageView.image = [UIImage imageNamed:@"vegetables32.png"];
        cell.backgroundColor = [UIColor colorWithRed:0.44 green:0.26 blue:0.08 alpha:0.25];

    } else {
        cell.imageView.image = nil;
        cell.backgroundColor = nil;
    }
    cell.textLabel.backgroundColor = [UIColor clearColor];
    cell.detailTextLabel.backgroundColor = [UIColor clearColor];
    return cell;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

-(void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath{
    NSManagedObjectContext *context = [self managedObjectContext];
    
    if (editingStyle == UITableViewCellEditingStyleDelete){
        [context deleteObject:[self.items objectAtIndex:indexPath.row]];
        
        NSError *error = nil;
        if(![context save:&error]){
            NSLog(@"Failed to remove! %@ %@", error, [error localizedDescription]);
            return;
        }
        
        [self.items removeObjectAtIndex:indexPath.row];
        [self.tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSManagedObject *item = [self.items objectAtIndex:indexPath.row];
    //NSLog(@"isListed = %@", [item valueForKey:@"isListed"]);
    [item setValue:@YES forKey:@"isListed"];
    //NSLog(@"isListed = %@", [item valueForKey:@"isListed"]);
    NSError *error = nil;
    
    NSManagedObjectContext *context = [self managedObjectContext];
    if(![context save:&error]){
        NSLog(@"Failed to remove! %@ %@", error, [error localizedDescription]);
        return;
    }
    
    [self.navigationController popToRootViewControllerAnimated:YES];
}

#pragma mark - SearchBar Method

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    NSManagedObjectContext *managedObjectContext = [self managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"Item"];

    if (searchText.length != 0) {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"itemName contains[c]%@", searchText];
        [fetchRequest setPredicate:predicate];
        self.items = [[managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
        [self.tableView reloadData];
    }
    else {
        self.items = [[managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
        [self.tableView reloadData];
    }
}

#pragma mark - Filter Method's

- (IBAction)abcFilterButton:(id)sender {
    NSManagedObjectContext *managedObjectContext = [self managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"Item"];
    if (self.abcIsPressed == NO) {
        NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"itemName" ascending:YES];
        fetchRequest.sortDescriptors = @[sortDescriptor];
        self.items = [[managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
        [self.tableView reloadData];
        self.abcIsPressed = YES;
    } else {
        NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"itemName" ascending:NO];
        fetchRequest.sortDescriptors = @[sortDescriptor];
        self.items = [[managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
        [self.tableView reloadData];
        self.abcIsPressed = NO;
    }
}

- (IBAction)categoryFilterButton:(id)sender {
    NSManagedObjectContext *managedObjectContext = [self managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"Item"];
    if (self.cgtIsPressed == NO) {
        NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"itemCategory" ascending:YES];
        fetchRequest.sortDescriptors = @[sortDescriptor];
        self.items = [[managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
        [self.tableView reloadData];
        self.cgtIsPressed = YES;
    } else {
        NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"itemCategory" ascending:NO];
        fetchRequest.sortDescriptors = @[sortDescriptor];
        self.items = [[managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
        [self.tableView reloadData];
        self.cgtIsPressed = NO;
    }
}

// Скрыть клавиатуру по нажатию в любом пустом месте экрана приложения
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    for (UIView* view in self.view.subviews)
        [view resignFirstResponder];
}

#pragma mark - Managed Object Context Method

-(NSManagedObjectContext *)managedObjectContext{
    NSManagedObjectContext *context = nil;
    id delegate = [[UIApplication sharedApplication] delegate];
    if ([delegate performSelector:@selector(managedObjectContext)]){
        context = [delegate managedObjectContext];
    }
    return context;
}

@end
