//
//  NewItemViewController.h
//  ShoppingList_0.1
//
//  Created by kravinov on 12/17/16.
//  Copyright © 2016 kravinov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NewItemViewController : UIViewController

@property (weak, nonatomic) IBOutlet UITextField *nameTextField;
@property (weak, nonatomic) IBOutlet UITextField *categoryTextField;
- (IBAction)saveItemButton:(id)sender;
- (IBAction)selectCategoryButton:(id)sender;

@end
