//
//  SWCatalogTableViewController.m
//  ShoppingList_0.1
//
//  Created by kravinov on 12/23/16.
//  Copyright © 2016 kravinov. All rights reserved.
//

#import "SWCatalogTableViewController.h"

@interface SWCatalogTableViewController ()

@end

@implementation SWCatalogTableViewController

#pragma mark - View Controller Lifecycle Methods

- (void)viewDidLoad {
    [super viewDidLoad];
    self.abcIsPressed = YES;
    self.cgtIsPressed = NO;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    NSManagedObjectContext *managedObjectContext = [self managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"Item"];
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"itemName" ascending:YES];
    fetchRequest.sortDescriptors = @[sortDescriptor];
    self.items = [[managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
    [self.tableView reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.items.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"Cell";
    
    SWCatalogTableViewCell *cell = (SWCatalogTableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];

    // Add utility buttons
    NSMutableArray *rightUtilityButtons = [NSMutableArray new];
    
    [rightUtilityButtons sw_addUtilityButtonWithColor:
     [UIColor colorWithRed:0.78f green:0.78f blue:0.8f alpha:1.0]
                                                title:@"Edit"];
    [rightUtilityButtons sw_addUtilityButtonWithColor:
     [UIColor colorWithRed:1.0f green:0.231f blue:0.188 alpha:1.0f]
                                                title:@"Delete"];
    
    cell.rightUtilityButtons = rightUtilityButtons;
    cell.delegate = self;
    
    NSManagedObject *item = [self.items objectAtIndex:indexPath.row];
    
    cell.textLabel.text = [item valueForKey:@"itemName"];
    cell.detailTextLabel.text = [item valueForKey:@"itemCategory"];
    
    if ([[item valueForKey:@"itemCategory"] isEqualToString:@"Выпечка"]) {
        cell.imageView.image = [UIImage imageNamed:@"bake32.png"];
        cell.backgroundColor = [UIColor colorWithRed:0.82 green:0.41 blue:0.12 alpha:0.25];
        
    } else if ([[item valueForKey:@"itemCategory"] isEqualToString:@"Морепродукты"]) {
        cell.imageView.image = [UIImage imageNamed:@"seafood32.png"];
        cell.backgroundColor = [UIColor colorWithRed:0.30 green:0.35 blue:0.40 alpha:0.25];
        
    } else if ([[item valueForKey:@"itemCategory"] isEqualToString:@"Фрукты"]) {
        cell.imageView.image = [UIImage imageNamed:@"fruits32.png"];
        cell.backgroundColor = [UIColor colorWithRed:0.89 green:0.15 blue:0.21 alpha:0.25];
        
    } else if ([[item valueForKey:@"itemCategory"] isEqualToString:@"Напитки"]) {
        cell.imageView.image = [UIImage imageNamed:@"beverages32.png"];
        cell.backgroundColor = [UIColor colorWithRed:0.18 green:0.55 blue:0.34 alpha:0.25];
        
    } else if ([[item valueForKey:@"itemCategory"] isEqualToString:@"Мясо"]) {
        cell.imageView.image = [UIImage imageNamed:@"meat32.png"];
        cell.backgroundColor = [UIColor colorWithRed:0.65 green:0.15 blue:0.04 alpha:0.25];
        
    } else if ([[item valueForKey:@"itemCategory"] isEqualToString:@"Овощи"]) {
        cell.imageView.image = [UIImage imageNamed:@"vegetables32.png"];
        cell.backgroundColor = [UIColor colorWithRed:0.44 green:0.26 blue:0.08 alpha:0.25];
        
    } else {
        cell.imageView.image = nil;
        cell.backgroundColor = nil;
    }
    cell.textLabel.backgroundColor = [UIColor clearColor];
    cell.detailTextLabel.backgroundColor = [UIColor clearColor];
    return cell;
}

- (NSArray *)rightButtons
{
    NSMutableArray *rightUtilityButtons = [NSMutableArray new];
    [rightUtilityButtons sw_addUtilityButtonWithColor:
     [UIColor colorWithRed:0.78f green:0.78f blue:0.8f alpha:1.0]
                                                title:@"Edit"];
    [rightUtilityButtons sw_addUtilityButtonWithColor:
     [UIColor colorWithRed:1.0f green:0.231f blue:0.188 alpha:1.0f]
                                                title:@"Delete"];
    
    return rightUtilityButtons;
}

- (NSArray *)leftButtons
{
    NSMutableArray *leftUtilityButtons = [NSMutableArray new];
    
    [leftUtilityButtons sw_addUtilityButtonWithColor:
     [UIColor colorWithRed:0.07 green:0.75f blue:0.16f alpha:1.0]
                                                icon:[UIImage imageNamed:@"check.png"]];
    [leftUtilityButtons sw_addUtilityButtonWithColor:
     [UIColor colorWithRed:1.0f green:1.0f blue:0.35f alpha:1.0]
                                                icon:[UIImage imageNamed:@"clock.png"]];
    [leftUtilityButtons sw_addUtilityButtonWithColor:
     [UIColor colorWithRed:1.0f green:0.231f blue:0.188f alpha:1.0]
                                                icon:[UIImage imageNamed:@"cross.png"]];
    [leftUtilityButtons sw_addUtilityButtonWithColor:
     [UIColor colorWithRed:0.55f green:0.27f blue:0.07f alpha:1.0]
                                                icon:[UIImage imageNamed:@"list.png"]];
    
    return leftUtilityButtons;
}


- (void)swipeableTableViewCell:(SWTableViewCell *)cell didTriggerRightUtilityButtonWithIndex:(NSInteger)index {
    switch (index) {
        case 0: {
            NSLog(@"More button was pressed");
            UIAlertView *errAlert = [[UIAlertView alloc] initWithTitle:@"Oops, простите Нас"
                                                               message:@"Здесь должен быть пеереход на экран редактирвания записи, но мы не успели его сделать :("
                                                              delegate:self
                                                     cancelButtonTitle:@"Cancel"
                                                     otherButtonTitles:@"OK", nil];
            [errAlert setAlertViewStyle:UIAlertViewStyleDefault];
            [errAlert show];
            break;
        }
        case 1:
        {
            NSManagedObjectContext *context = [self managedObjectContext];
            
            NSIndexPath *cellIndexPath = [self.tableView indexPathForCell:cell];
            
            [context deleteObject:[self.items objectAtIndex:cellIndexPath.row]];
            
            NSError *error = nil;
            if(![context save:&error]){
                NSLog(@"Failed to remove! %@ %@", error, [error localizedDescription]);
                return;
            }
            
            [self.items removeObjectAtIndex:cellIndexPath.row];
            [self.tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:cellIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
        }
        default:
            break;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSManagedObject *item = [self.items objectAtIndex:indexPath.row];
    //NSLog(@"isListed = %@", [item valueForKey:@"isListed"]);
    [item setValue:@YES forKey:@"isListed"];
    //NSLog(@"isListed = %@", [item valueForKey:@"isListed"]);
    NSError *error = nil;
    
    NSManagedObjectContext *context = [self managedObjectContext];
    if(![context save:&error]){
        NSLog(@"Failed to remove! %@ %@", error, [error localizedDescription]);
        return;
    }
    
    [self.navigationController popToRootViewControllerAnimated:YES];
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
#pragma mark - SearchBar Method

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    NSManagedObjectContext *managedObjectContext = [self managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"Item"];
    
    if (searchText.length != 0) {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"itemName contains[c]%@", searchText];
        [fetchRequest setPredicate:predicate];
        self.items = [[managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
        [self.tableView reloadData];
    }
    else {
        self.items = [[managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
        [self.tableView reloadData];
    }
}

#pragma mark - Filter Method's

- (IBAction)abcFilterButton:(id)sender {
    NSManagedObjectContext *managedObjectContext = [self managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"Item"];
    if (self.abcIsPressed == NO) {
        NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"itemName" ascending:YES];
        fetchRequest.sortDescriptors = @[sortDescriptor];
        self.items = [[managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
        [self.tableView reloadData];
        self.abcIsPressed = YES;
    } else {
        NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"itemName" ascending:NO];
        fetchRequest.sortDescriptors = @[sortDescriptor];
        self.items = [[managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
        [self.tableView reloadData];
        self.abcIsPressed = NO;
    }
}

- (IBAction)categoryFilterButton:(id)sender {
    NSManagedObjectContext *managedObjectContext = [self managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"Item"];
    if (self.cgtIsPressed == NO) {
        NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"itemCategory" ascending:YES];
        fetchRequest.sortDescriptors = @[sortDescriptor];
        self.items = [[managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
        [self.tableView reloadData];
        self.cgtIsPressed = YES;
    } else {
        NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"itemCategory" ascending:NO];
        fetchRequest.sortDescriptors = @[sortDescriptor];
        self.items = [[managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
        [self.tableView reloadData];
        self.cgtIsPressed = NO;
    }
}

#pragma mark - Managed Object Context Method

-(NSManagedObjectContext *)managedObjectContext{
    NSManagedObjectContext *context = nil;
    id delegate = [[UIApplication sharedApplication] delegate];
    if ([delegate performSelector:@selector(managedObjectContext)]){
        context = [delegate managedObjectContext];
    }
    return context;
}

@end
